# building stage
FROM maven:3-jdk-8 AS build

WORKDIR /app
COPY app .
RUN [ "mvn", "package" ]

# list dependencies stage.
FROM maven:3-jdk-8 AS dependencies

WORKDIR /app
COPY --from=build /app .
CMD [ "mvn", "dependency:tree" ]

# running stage
FROM tomcat:9-jdk8

COPY --from=build /app/target/struts-example-app.war /usr/local/tomcat/webapps/
